package com.epam.snakesAndLaddersApp.service

import com.epam.snakesAndLaddersApp.model.GameTest.Companion.getPosition
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class GameServiceTest(
        @Autowired private val gameService: GameService
) {

    @Test
    fun createGameReturnsNewGame() {
        val game = gameService.createGame()
        assertNotNull(game)
    }

    @Test
    fun savedGameHasId() {
        val game = gameService.createGame()
        assertNotNull(game.id)
    }

    @Test
    fun savedGameCanBeLoaded() {
        val game = gameService.createGame()
        val savedGame = gameService.getGame(game.id!!)
        assertNotNull(savedGame)
    }

    @Test
    fun gameSavedAfterTurn() {
        val points = 5
        val game = gameService.createGame()
        val savedGame = gameService.makeTurn(game.id!!, points)
        assertNotNull(savedGame)
    }

    @Test
    fun initialTokensPositionIs1() {
        val initialPosition = 1
        val game = gameService.createGame()
        for (tokenMeta in game.tokens.values) {
            assertEquals(initialPosition, tokenMeta.currentPosition,
                    """
                        Given the game is started
                        When the token is placed on the board
                        Then the token is on square 1
                        """.trimIndent())
        }
    }

    @Test
    fun updatedPositionIsCorrect() {
        val points = 3
        val game = gameService.createGame()
        val savedGame = gameService.makeTurn(game.id!!, points)
        val expectedPosition = 4
        assertEquals(expectedPosition, savedGame.tokens.values.first().currentPosition,
                """
                    Given the token is on square 1
                    When the token is moved 3 spaces
                    Then the token is on square 4
                    """.trimIndent())
    }

    @Test
    fun updatedTwoTimesPositionIsCorrect() {
        val firstMove = 3
        val secondMove = 4
        val game = gameService.createGame()
        gameService.makeTurn(game.id!!, firstMove)
        val savedGame = gameService.makeTurn(game.id!!, secondMove)
        val expectedPosition = 8
        assertEquals(expectedPosition, savedGame.tokens.values.first().currentPosition,
                """
                    Given the token is on square 1
                    When the token is moved 3 spaces
                    And then it is moved 4 spaces
                    Then the token is on square 8
                    """.trimIndent())
    }

    @Test
    fun useRollTheDiceIfPointsNotDefined() {
        val game = gameService.createGame()
        val currentPosition = getPosition(game)
        val updatedGame = gameService.makeTurn(game.id!!)
        val updatedPosition = getPosition(updatedGame)
        assertTrue(currentPosition < updatedPosition)
    }

    @Test
    fun randomPointsShouldBeBetween1And6Inclusive() {
        val game = gameService.createGame()
        var currentPosition = getPosition(game)
        for (attempt in 1..10) {
            val updatedGame = gameService.makeTurn(game.id!!)
            val updatedPosition = getPosition(updatedGame)
            val randomPoints = updatedPosition - currentPosition

            assertTrue(randomPoints in 1..6, """
     Given the game is started
     When the player rolls a die
     Then the result should be between 1-6 inclusive, but was: "$randomPoints"
     """.trimIndent())

            currentPosition = updatedPosition
        }
    }

}