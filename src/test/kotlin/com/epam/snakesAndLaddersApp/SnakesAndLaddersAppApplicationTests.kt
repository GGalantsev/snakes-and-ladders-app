package com.epam.snakesAndLaddersApp

import com.epam.snakesAndLaddersApp.controller.GameController
import com.epam.snakesAndLaddersApp.factory.GameFactory
import com.epam.snakesAndLaddersApp.service.GameService
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class SnakesAndLaddersAppApplicationTests(
        @Autowired
        private val gameController: GameController?,
        @Autowired
        private val gameService: GameService?,
        @Autowired
        private val gameFactory: GameFactory?
) {

    @Test
    fun contextLoads() {
        assertNotNull(gameController)
        assertNotNull(gameService)
        assertNotNull(gameFactory)
    }

}
