package com.epam.snakesAndLaddersApp.controller

import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class GameControllerTest(
        @Autowired private val gameController: GameController
) {

    @Test
    fun createGameReturnsNewGame() {
        val createGame = gameController.createGame()
        assertNotNull(createGame.body)
    }

    @Test
    fun getGameReturnsGame() {
        val game = gameController.createGame().body!!
        val savedGame = gameController.getGame(game.id!!)
        assertNotNull(savedGame.body)
    }

    @Test
    fun makeTurnReturnsGame() {
        val game = gameController.createGame().body!!
        val updatedGame = gameController.makeTurn(game.id!!, 5)
        assertNotNull(updatedGame.body)
    }

    @Test
    fun makeTurnCanUseRandomPoints() {
        val game = gameController.createGame().body!!
        val updatedGame = gameController.makeTurn(game.id!!, null)
        assertNotNull(updatedGame.body)
    }
}