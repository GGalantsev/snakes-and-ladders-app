package com.epam.snakesAndLaddersApp.factory

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class GameFactoryTest(
        @Autowired private val gameFactory: GameFactory
) {

    @Test
    fun gameCanBeCreated() {
        val game = gameFactory.createGame(tokensAmount = 1)
        Assertions.assertNotNull(game)
    }

    @Test
    fun tokensCanBeCreated() {
        val game = gameFactory.createGame(tokensAmount = 1)
        Assertions.assertNotNull(game.tokens)
    }

    @Test
    fun tokenMetaDefaultPositionIs1() {
        val startPosition = 1
        val game = gameFactory.createGame(tokensAmount = 1)
        Assertions.assertEquals(game.tokens.values.first().currentPosition, startPosition,
                "Token should start the game from the first cell")
    }

    @Test
    fun boardCanBeCreated() {
        val game = gameFactory.createGame(tokensAmount = 1)
        Assertions.assertNotNull(game.board)
    }

    @Test
    fun tokensAmountIsCorrect() {
        val amount = 1
        val game = gameFactory.createGame(tokensAmount = amount)
        Assertions.assertEquals(amount, game.tokens.keys.size, "Tokens amount not equal to defined one")
    }

    @Test
    fun tokensAmountIsCorrect2() {
        val amount = 3
        val game = gameFactory.createGame(tokensAmount = amount)
        Assertions.assertEquals(amount, game.tokens.keys.size, "Tokens amount not equal to defined one")
    }

}