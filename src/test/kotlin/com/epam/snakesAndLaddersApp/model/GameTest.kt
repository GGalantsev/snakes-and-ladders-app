package com.epam.snakesAndLaddersApp.model

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class GameTest {

    @Test
    fun gameCanBeCreated() {
        val game = createGame()
        assertNotNull(game)
    }

    @Test
    fun getTokens() {
        val game = createGame()
        assertNotNull(game.tokens)
    }

    @Test
    fun getToken() {
        val game = createGame()
        assertNotNull(game.tokens.keys.first())
    }

    @Test
    fun getTokenMeta() {
        val game = createGame()
        assertNotNull(game.tokens.values.first())
    }

    @Test
    fun checkNewGameTokenMetaPosition() {
        val game = createGame()
        assertEquals(game.tokens.values.first().currentPosition, 1, "Initial token position must be '1'.")
    }

    @Test
    fun getCurrentToken() {
        val game = createGame()
        assertNotNull(game.currentTokenId)
    }

    @Test
    fun getBoard() {
        val game = createGame()
        assertNotNull(game.board)
    }

    @Test
    fun tokenCanBeMoved() {
        val points = 5
        val game = createGame()
        val gameAfterTurn = game.makeTurn(points)
        val expectedPosition = 6
        assertEquals(expectedPosition, gameAfterTurn.tokens.values.first().currentPosition,
                "when first move is 5 - position should be 6")
    }

    @Test
    fun tokenCanBeMovedBy4() {
        val points = 4
        val game = createGame()
        val currentPosition = getPosition(game)
        val gameAfterTurn = game.makeTurn(points)
        val updatedPosition = getPosition(gameAfterTurn)
        val expectedDistance = 4
        val distance = updatedPosition - currentPosition
        assertEquals(expectedDistance, distance,
                """
                    Given the player rolls a 4
                    When they move their token
                    Then the token should move 4 spaces
                    """.trimIndent())
    }

    @Test
    fun playerCanWinTheGame() {
        val tokenId = 1
        val currentPosition = 97
        val pointsToWin = 3
        val game = Game(
                id = 1,
                tokens = mapOf(tokenId to TokenMeta(currentPosition)),
                currentTokenId = tokenId,
                board = createBoard()
        )
        val gameAfterTurn = game.makeTurn(pointsToWin)
        assertNotNull(gameAfterTurn.winnerTokenId)
        assertEquals(tokenId, gameAfterTurn.winnerTokenId,
                """
                    Given the token is on square 97
                    When the token is moved 3 spaces
                    Then the token is on square 100
                    And the player has won the game
                    """.trimIndent())
    }

    @Test
    fun playerDoNotWinTheGameIfCurrentPositionMoreThanBoardSize() {
        val tokenId = 1
        val currentPosition = 97
        val pointsToWin = 4
        val game = Game(
                id = 1,
                tokens = mapOf(tokenId to TokenMeta(currentPosition)),
                currentTokenId = tokenId,
                board = createBoard()
        )
        val gameAfterTurn = game.makeTurn(pointsToWin)
        assertNull(gameAfterTurn.winnerTokenId)
        assertEquals(currentPosition, getPosition(gameAfterTurn),
                """
                    Given the token is on square 97
                    When the token is moved 3 spaces
                    Then the token is on square 100
                    And the player has won the game
                    """.trimIndent())
    }

    private fun createGame(): Game {
        val tokenId = 1
        return Game(
                id = 1,
                tokens = mapOf(tokenId to createTokenMeta()),
                currentTokenId = tokenId,
                board = createBoard()
        )
    }

    companion object {
        private fun createTokenMeta() = TokenMeta()
        private fun createBoard() = Board()
        fun getPosition(game: Game) = game.tokens.values.first().currentPosition
    }

}