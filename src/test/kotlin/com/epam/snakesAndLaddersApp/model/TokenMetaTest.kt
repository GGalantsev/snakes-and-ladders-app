package com.epam.snakesAndLaddersApp.model

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

internal class TokenMetaTest {

    @Test
    fun tokenMetaCanBeCreated() {
        val tokenMeta = TokenMeta(2)
        assertNotNull(tokenMeta)
    }

    @Test
    fun tokenMetaDefaultPositionIs1() {
        val tokenMeta = TokenMeta()
        val startPosition = 1;
        assertEquals(tokenMeta.currentPosition, startPosition, "Token should start the game from the first cell")
    }
}