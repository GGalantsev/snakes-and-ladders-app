package com.epam.snakesAndLaddersApp.model

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

internal class BoardTest {

    @Test
    fun boardCanBeCreated() {
        val board = Board(50)
        assertNotNull(board)
    }

    @Test
    fun boardDefaultLengthIs100() {
        val board = Board()
        val defaultBoardSize = 100
        assertEquals(board.length, defaultBoardSize, "Default board size must be $defaultBoardSize.")
    }
}