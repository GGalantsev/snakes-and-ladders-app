package com.epam.snakesAndLaddersApp.repository

import com.epam.snakesAndLaddersApp.model.Game

interface GameRepository {
    fun get(id: Int): Game
    fun save(game: Game): Game
}
