package com.epam.snakesAndLaddersApp.repository

import com.epam.snakesAndLaddersApp.model.Game
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Repository
import org.springframework.web.client.HttpClientErrorException

@Repository
class DummyGameRepository : GameRepository {
    private val map = mutableMapOf<Int, Game>()

    override fun get(id: Int): Game {
        return map.getOrElse(id) { throw HttpClientErrorException(HttpStatus.NOT_FOUND, "GameNotFound") }
    }


    override fun save(game: Game): Game {
        return if (game.id != null) {
            map.put(game.id, game)
            map.get(game.id)!!
        } else {
            val generatedId: Int = (map.keys.maxOrNull() ?: 0) + 1
            map.put(generatedId, game.copy(id = generatedId))
            map.get(generatedId)!!
        }
    }

}