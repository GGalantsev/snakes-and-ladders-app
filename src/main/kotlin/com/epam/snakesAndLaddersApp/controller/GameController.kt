package com.epam.snakesAndLaddersApp.controller

import com.epam.snakesAndLaddersApp.model.Game
import com.epam.snakesAndLaddersApp.service.GameService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/game")
class GameController(
        @Autowired private val gameService: GameService
) {

    @PostMapping("/")
    fun createGame(): HttpEntity<Game> {
        val game = gameService.createGame()
        return ResponseEntity.ok(game)
    }

    @GetMapping("/{id}")
    fun getGame(
            @PathVariable id: Int
    ): HttpEntity<Game> {
        val game = gameService.getGame(id)
        return ResponseEntity.ok(game)
    }

    @PostMapping("/{id}/turn", "/{id}/turn/{points}")
    fun makeTurn(
            @PathVariable id: Int,
            @PathVariable(required = false) points: Int?
    ): HttpEntity<Game> {
        val game = points
                ?.let { gameService.makeTurn(id, it) }
                ?: gameService.makeTurn(id)
        return ResponseEntity.ok(game)
    }
}