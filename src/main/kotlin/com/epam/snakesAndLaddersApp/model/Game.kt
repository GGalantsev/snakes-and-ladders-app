package com.epam.snakesAndLaddersApp.model

/**
 * Representation of the current state for the Snakes And Ladders game
 */
data class Game(

        /**
         * Game identifier
         */
        val id: Int? = null,

        /**
         * Map of tokens ids to tokens metadata to represent their state in current game
         */
        val tokens: Map<Int, TokenMeta>,

        /**
         * Token Id, that must be used for the next turn
         */
        val currentTokenId: Int,

        /**
         * Token, that reached [board] length earlier than others
         */
        val winnerTokenId: Int? = null,

        /**
         * Board used in current game
         */
        val board: Board

) {

    /**
     * Moves token with [currentTokenId] by [points] amount
     * @return copy of [Game] with changes
     */
    fun makeTurn(points: Int): Game {
        val tokenMeta = tokens[currentTokenId]
        val currentPosition = tokenMeta!!.currentPosition
        val newPosition = currentPosition + points
        val updatedPosition = if (newPosition > board.length) currentPosition else newPosition

        val nextTokenId = if (tokens.keys.contains(currentTokenId + 1)) currentTokenId + 1 else 1

        return this.copy(
                tokens = tokens + Pair(currentTokenId, tokenMeta.copy(currentPosition = updatedPosition)),
                currentTokenId = nextTokenId,
                winnerTokenId = if (updatedPosition == board.length) currentTokenId else null
        )
    }

}