package com.epam.snakesAndLaddersApp.model

/**
 * Representation of the board from the Snakes And Ladders game
 */
data class Board(

        /**
         * Amount of desired points to win the game
         */
        val length: Int = 100

)