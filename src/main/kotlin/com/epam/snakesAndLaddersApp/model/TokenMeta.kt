package com.epam.snakesAndLaddersApp.model

/**
 * Information about token in the [Game]
 */
data class TokenMeta(

        /**
         * Current position of the token on the [Board]
         */
        val currentPosition: Int = 1

)