package com.epam.snakesAndLaddersApp.factory

import com.epam.snakesAndLaddersApp.model.Board
import com.epam.snakesAndLaddersApp.model.Game
import com.epam.snakesAndLaddersApp.model.TokenMeta
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class GameFactory(
        @Value("\${board.length:100}")
        private val boardLength: Int = 100
) {

    /**
     * @return new [Game] with initial state of tokens and game configuration
     */
    fun createGame(tokensAmount: Int): Game {
        val tokens = createTokens(tokensAmount)
        val board = createBoard()
        return Game(tokens = tokens, board = board, currentTokenId = tokens.keys.first())
    }

    /**
     * @return [Map] of token ids to their initial [TokenMeta]data for required [amount] of tokens
     */
    private fun createTokens(amount: Int) =
            (1..amount).map { id -> id to TokenMeta() }.toMap()

    /**
     * @return game [Board] with defined length
     */
    private fun createBoard(length: Int = boardLength): Board =
            Board(length)
}