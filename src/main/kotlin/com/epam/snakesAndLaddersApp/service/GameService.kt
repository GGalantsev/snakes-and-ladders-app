package com.epam.snakesAndLaddersApp.service

import com.epam.snakesAndLaddersApp.factory.GameFactory
import com.epam.snakesAndLaddersApp.model.Game
import com.epam.snakesAndLaddersApp.repository.GameRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class GameService(
        @Autowired private val gameFactory: GameFactory,
        @Autowired private val gameRepository: GameRepository
) {

    /**
     * Create a new game
     * @return [Game], saved in repository
     */
    fun createGame(): Game {
        val game = gameFactory.createGame(1)
        return gameRepository.save(game)
    }

    /**
     * @return [Game], saved in repository identified by [id]
     */
    fun getGame(id: Int): Game {
        return gameRepository.get(id)
    }

    /**
     * Performs a turn for current player in game with [id]
     * using points from [rollTheDice]
     * @return changed [Game], saved in repository
     */
    fun makeTurn(id: Int): Game {
        return makeTurn(id, rollTheDice())
    }

    /**
     * Performs a turn for current token in game with [id] and [points] length
     * @return changed [Game], saved in repository
     */
    fun makeTurn(id: Int, points: Int): Game {
        val game = gameRepository.get(id)
        val gameAfterTurn = game.makeTurn(points)
        return gameRepository.save(gameAfterTurn)
    }

    private fun rollTheDice(): Int = (Math.random() * 6 + 1).toInt()

}
