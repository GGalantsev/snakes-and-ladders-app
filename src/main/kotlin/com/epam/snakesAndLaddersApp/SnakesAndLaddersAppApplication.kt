package com.epam.snakesAndLaddersApp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SnakesAndLaddersAppApplication

fun main(args: Array<String>) {
	runApplication<SnakesAndLaddersAppApplication>(*args)
}
