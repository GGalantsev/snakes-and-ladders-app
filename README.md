# Snakes and Ladders REST app
Reference:
> http://agilekatas.co.uk/katas/SnakesAndLadders-Kata

Implemented:
> Feature 1 - Moving Your Token

## Endpoints:
### Create game [POST]
- http://localhost:8080/game/
#### Request Body: empty
```bash
curl --header "Content-Type: application/json" --request POST --data '' http://localhost:8080/game/
```
#### Response Body:
```json
{
  "id": 1,
  "tokens": {
    "1": {
      "currentPosition": 1
    }
  },
  "currentTokenId": 1,
  "winnerTokenId": null,
  "board": {
    "length": 100
  }
}
```

### Get game [GET]
- http://localhost:8080/game/{id}
```bash
curl --header "Content-Type: application/json" --request GET http://localhost:8080/game/1
```
#### Response Body:
```json
{
  "id": 1,
  "tokens": {
    "1": {
      "currentPosition": 1
    }
  },
  "currentTokenId": 1,
  "winnerTokenId": null,
  "board": {
    "length": 100
  }
}
```

### Make turn [post]
- http://localhost:8080/game/{id}/turn/{points}
- http://localhost:8080/game/{id}/turn (random points from 1 to 6)
#### Request Body: empty
```bash
curl --header "Content-Type: application/json" --request POST --data '' http://localhost:8080/game/1/turn/5
```
#### Response Body:
```json
{
  "id": 1,
  "tokens": {
    "1": {
      "currentPosition": 6
    }
  },
  "currentTokenId": 1,
  "winnerTokenId": null,
  "board": {
    "length": 100
  }
}
```
